Prometheus Helm Chart taken from:

https://github.com/kubernetes/charts/tree/1230e8a4ed45b66fb00f19bea06b38e7f6944d9e/stable/prometheus

With the following modifications:

```
diff -r chart/values.yaml ../charts/stable/prometheus/values.yaml
283c283
<   enabled: false
---
>   enabled: true
541,542c541
<     type: NodePort
<     nodePort: 30010
---
>     type: ClusterIP
675,679d673
<     global:
<       scrape_interval:     20s
<       evaluation_interval: 30s
<       # scrape_timeout is set to the global default (10s).
< 
682,688c676,678
<         honor_labels: true
< 
<        # metrics_path defaults to '/metrics'
<        # scheme defaults to 'http'.
< 
<         consul_sd_configs:
<         - server: 'production-consul-ui.consul:8500'
---
>         static_configs:
>           - targets:
>             - localhost:9090
```
